﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHome.ViewModel
{
   public class ButtonsViewModal
    {
        public DelegateCommand OpenedPopupBoxCommand { get; set; }
        public DelegateCommand ClosedPopupBoxCommand { get; set; }

        public ButtonsViewModal()
        {
            OpenedPopupBoxCommand = new DelegateCommand(o => OpenPopupBox());

            ClosedPopupBoxCommand = new DelegateCommand(o => ClosePopupBox());
        }

        private void OpenPopupBox()
        {
            System.Windows.Forms.MessageBox.Show("Opened PopupBox");
        }
        private void ClosePopupBox()
        {
            System.Windows.Forms.MessageBox.Show("Closed PopupBox");
        }
    }
}
