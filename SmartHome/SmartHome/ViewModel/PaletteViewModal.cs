﻿using MaterialDesignColors;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SmartHome.ViewModel
{
   public class PaletteViewModal:ObservableObject
    {
        public IEnumerable<Swatch> Swatches { get; set; }
        public PaletteViewModal()
        {
            Swatches = new SwatchesProvider().Swatches;
        }

        public ICommand SetThemeCommand
        {
            get { return new DelegateCommand(o => ChangeTheme((bool)o)); }

        }

        private void ChangeTheme(bool IsDark)
        {
            new PaletteHelper().SetLightDark(IsDark);
        }

        public ICommand ApplyPrimaryCommand { get; } = new DelegateCommand(o => ApplyPrimary((Swatch)o));

        public ICommand ApplyAccentCommand { get; } = new DelegateCommand(o => ApplyAccent((Swatch)o));

            private static void ApplyPrimary(Swatch swatch)
        {
            new PaletteHelper().ReplacePrimaryColor(swatch);
        }
        private static void ApplyAccent(Swatch swatch)
        {
            new PaletteHelper().ReplaceAccentColor(swatch);
        }
    }
}
