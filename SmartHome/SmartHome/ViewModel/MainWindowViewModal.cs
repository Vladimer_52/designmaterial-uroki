﻿using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartHome.Helpers;

namespace SmartHome.ViewModel
{
    public class MainWindowViewModal
    {
        MainWindow window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

        public MainWindowViewModal()
        {
            OpenPaletteCommand = new DelegateCommand(o => OpenPalette());
            OpenButtonsCommand = new DelegateCommand(o => OpenButtons());
        }
        #region Command

        public DelegateCommand OpenPaletteCommand { get; set; }
        public DelegateCommand OpenButtonsCommand { get; set; }
        #endregion

        #region CommandImplemetarion

        private void OpenPalette()
        {
            window.mainGrid.Children.Clear();
            window.mainGrid.Children.Add(new View.Pallete());
        }

        private void OpenButtons()
        {
            window.mainGrid.Children.Clear();
            window.mainGrid.Children.Add(new View.Buttons());
        }
        #endregion
    }
}
