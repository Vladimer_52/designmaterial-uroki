﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SmartHome
{
    public class DelegateCommand : ICommand
    {
        #region Fields

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion

        #region Consructors

        public DelegateCommand(Action<object> execute):this(execute, null) { }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if(execute == null) {
                throw new ArgumentException("execute");
                    }
            _execute = execute;
            _canExecute = canExecute;
        }
        #endregion

        #region ICommandMembers

        public bool CanExecute(object param)
        {
            return _canExecute?.Invoke(param) ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object param)
        {
            _execute(param);
        }
        
        #endregion
    }
}
