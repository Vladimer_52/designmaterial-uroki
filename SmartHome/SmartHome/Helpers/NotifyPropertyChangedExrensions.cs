﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SmartHome.Helpers
{
    public static class NotifyPropertyChangedExrensions
    {
        public static void Mutateverbose<Tfield>(this INotifyPropertyChanged instance, ref Tfield field, Tfield newValue, Action<PropertyChangedEventArgs> raise,
                                                [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<Tfield>.Default.Equals(field, newValue)) return;
            field = newValue;
            raise?.Invoke(new PropertyChangedEventArgs(propertyName));
        }
    }
}
