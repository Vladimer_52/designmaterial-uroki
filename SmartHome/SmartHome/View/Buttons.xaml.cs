﻿using SmartHome.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartHome.View
{
    /// <summary>
    /// Логика взаимодействия для Buttons.xaml
    /// </summary>
    public partial class Buttons : UserControl
    {
        public Buttons()
        {
            InitializeComponent();
            DataContext = new ButtonsViewModal();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if((CountingBadge.Badge == null) || Equals(CountingBadge, ""))
            {
                CountingBadge.Badge = 0;

            }
            var next = int.Parse(CountingBadge.Badge.ToString()) + 1;

            CountingBadge.Badge = next < 10 ? (object)next : null;

        }
    }
}
